/*
*
*	Libraries.
*
*/
const { Command } = require('discord.js-commando');
const { NewsFeed } = require('../../libs/cso/news');
const { keys, forEach, capitalize, isEmpty, isEqual } = require('lodash');


/*
*
*	Exports.
*
*/
module.exports = class NewsChannelsCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'channels',
			group: 'news',
			memberName: 'channels',
			description: 'Gets region subscribers.',
			examples: ['channels', 'channels steam'],
			guildOnly: true,
			clientPermissions: ['VIEW_CHANNEL', 'SEND_MESSAGES'],
			userPermissions: ['ADMINISTRATOR'],
			throttling: {
				usages: parseInt (process.env.DISCORD_COMMAND_CHANNELS_THROTTLING_USAGE),
				duration: parseInt (process.env.DISCORD_COMMAND_CHANNELS_THROTTLING_DURATION),
			},

			args: [
				{
					key: 'region',
					label: 'region',
					prompt: `What region would you like to retrieve? **all,${keys (NewsFeed.news)}**`,
					type: 'regionchannel',
				},
			],
		});
	}


	async run(msg, args) {
		const region = args.region;
		const guild = msg.guild;
		const regions = (isEmpty(region) || isEqual(region, '*') || isEqual(region, 'all')) ? keys (NewsFeed.news) : [region];

		msg.say(`Getting \`${region}\` subscribers...`);

		for (const regionName of regions) {
			let strBuf = `${capitalize(regionName)} news:`;
			forEach (await NewsFeed.news[regionName].getSubscriber (guild), (channelId) => {
				const channel = guild.channels.get (channelId);
				if (channel) {
					strBuf += '\n';
					strBuf += `> ${channel}`;
				}
			});
			msg.say(`${strBuf}\n.\n.`);
		}

		return msg.say(`Getting \`${region}\` subscribers...Done`);
	}
};
