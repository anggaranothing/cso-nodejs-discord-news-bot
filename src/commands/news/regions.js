/*
*
*	Libraries.
*
*/
const { Command } = require('discord.js-commando');
const { NewsFeed } = require('../../libs/cso/news');
const { keys } = require('lodash');


/*
*
*	Exports.
*
*/
module.exports = class NewsRegionsCommand extends Command {
	constructor(client) {
		super(client, {
			name: 'regions',
			group: 'news',
			memberName: 'regions',
			description: 'Gets all news regions.',
			guildOnly: true,
			clientPermissions: ['VIEW_CHANNEL', 'SEND_MESSAGES'],
			userPermissions: ['ADMINISTRATOR'],
		});
	}


	run(msg) {
		return msg.say (`Available news region(s): **${keys (NewsFeed.news)}**`);
	}
};
