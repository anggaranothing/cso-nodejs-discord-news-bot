/*
*
*	Libraries.
*
*/
const MarkdownNews = require('../markdown');
const { all, get } = require('axios').default;
const { load } = require('cheerio');
const { split, sortBy, uniqWith, slice, differenceWith } = require('lodash');
const { URL, Comparator } = require('../../utils');


/**
 * Nexon (Korea) News.
 */
class NexonNews extends MarkdownNews {
	constructor(client) {
		super(client, 'nexon');
	}


	async isOutdated() {
		let result = [];
		const self = this;

		return all ([
			get (process.env.NEXON_NEWS_SYSTEM_URL, { responseType: 'text' }),
			get (process.env.NEXON_NEWS_UPDATE_URL, { responseType: 'text' }),
			get (process.env.NEXON_NEWS_DEV_URL, { responseType: 'text' }),
			get (process.env.NEXON_NEWS_EVENT_URL, { responseType: 'text' }),
		])
			.then ((responses) => {
				for (let i = 0; i <= 1; i++) {
					const $ = load (responses[i].data);
					$('li', 'ul.list._skin4').each((_id, elm) => {
						result.push ({
							title: $('a', 'span.noti_tit', elm).text(),
							date: new Date ($('span.time', 'div.noti_info', elm).remove('em').text()),
						});
					});
				}
				return [responses[2].data, responses[3].data];
			})
			.then ((arrData) => {
				for (const resData of arrData) {
					const $ = load (resData);
					$('li', 'ul.list._skin2').each((_id, elm) => {
						result.push ({
							title: $('a', 'span.tit._news', elm).text(),
							date: new Date (split ($('span.time', 'div.gr_tit', elm).remove('em').text(), '~')[0].replace (' ', '')),
						});
					});
				}
			})
			.then (async () => {
				const cache = await self.retrieveCache();
				result = sortBy (result, Comparator.Article.byDate);
				result = slice (result, Math.max (0, result.length - parseInt(process.env.NEXON_NEWS_ARTICLE_LIMIT)));
				return differenceWith (result, cache.articles, Comparator.Article.byTitle).length > 0;
			})
	}


	async retrieve(asJson) {
		let result = [];
		const baseurl = process.env.NEXON_NEWS_SYSTEM_URL;
		const links = [];

		return all ([
			get(process.env.NEXON_NEWS_SYSTEM_URL, { responseType: 'text' }),
			get(process.env.NEXON_NEWS_UPDATE_URL, { responseType: 'text' }),
			get(process.env.NEXON_NEWS_DEV_URL, { responseType: 'text' }),
			get(process.env.NEXON_NEWS_EVENT_URL, { responseType: 'text' }),
		])
			.then ((responses) => {
				for (let i = 0; i <= 1; i++) {
					const $ = load (responses[i].data);
					$('li', 'ul.list._skin4').each((_id, elm) => {
						const linkElm = $('a', 'span.noti_tit', elm);
						const title = linkElm.text();
						const link = URL.repairUrl (linkElm.attr('href'), baseurl);
						const date = $('span.time', 'div.noti_info', elm).remove('em').text();

						links.push (link);
						result.push ({
							title: title,
							date: new Date (date),
							author: process.env.NEXON_NEWS_AUTHOR,
							url: link,
							icon: process.env.NEXON_NEWS_ICON_URL,
							thumbnail: process.env.NEXON_NEWS_THUMBNAIL_URL,
						});
					});
				}
				return [responses[2].data, responses[3].data];
			})
			.then ((arrData) => {
				const $ = load (arrData[0]);
				$('li', 'ul.list._skin2').each((_id, elm) => {
					const linkElm = $('a', 'span.tit._news', elm);
					const title = linkElm.text();
					const link = URL.repairUrl (linkElm.attr('href'), baseurl);
					const date = $('span.time', 'div.gr_tit', elm).remove('em').text();

					result.push ({
						title: title,
						date: new Date (date),
						author: process.env.NEXON_NEWS_AUTHOR,
						url: link,
						icon: process.env.NEXON_NEWS_ICON_URL,
						thumbnail: $('img[onerror]', 'div.gr_img', elm).attr('src') || process.env.NEXON_NEWS_THUMBNAIL_URL,
						content: ''
					});
				});
				return arrData[1];
			})
			.then ((data) => {
				const $ = load (data);
				$('li', 'ul.list._skin2').each((_id, elm) => {
					const linkElm = $('a', 'span.tit._news', elm);
					const title = linkElm.text();
					const link = URL.repairUrl (linkElm.attr('href'), baseurl);
					const dates = split ($('span.time', 'div.gr_tit', elm).remove('em').text(), '~');
					const startDate = dates[0].replace (' ', '');
					const endDate = dates[1].replace (' ', '');
					const isExpired = $('div.gr_state', elm).hasClass('_done');

					result.push ({
						title: title,
						date: new Date (startDate),
						author: process.env.NEXON_NEWS_AUTHOR,
						url: !isExpired ? link : process.env.NEXON_NEWS_EVENT_URL,
						icon: process.env.NEXON_NEWS_ICON_URL,
						thumbnail: $('img[onerror]', 'div.gr_img', elm).attr('src') || process.env.NEXON_NEWS_THUMBNAIL_URL,
						content: `.\n.\n.\nStart date: ${startDate}\nEnd date: ${endDate}\n.\n.\n.`,
					});
				});
			})
			.then(async () => {
				for (let index = 0; index < links.length; index++) {
					const response = await get(links[index], { responseType: 'text' });
					const $ = load(response.data);
					const contents = $('div.board_con', 'div.content_area').remove('map');
					$('img[src]', contents).each((id, elm) => {
						const src = URL.repairUrl($(elm).attr('src'), baseurl);
						if (id === 0) result[index].thumbnail = src;
						$(elm).replaceWith(src);
					});
					$('a', contents).each((_id, elm) => {
						$(elm).attr('href', URL.repairUrl($(elm).attr('href')));
					});
					result[index].content = contents.html() || '';
				}

				result = uniqWith (result, Comparator.Article.isEquals);
				result = sortBy (result, Comparator.Article.byDate);
				result = slice (result, Math.max (0, result.length - parseInt(process.env.NEXON_NEWS_ARTICLE_LIMIT)));
				if (asJson) result = JSON.stringify (result);
				return result;
			});
	}
}


module.exports = NexonNews;
