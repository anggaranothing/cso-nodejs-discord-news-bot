/*
*
*	Libraries.
*
*/
const MarkdownNews = require('../markdown');
const { get, all, spread } = require('axios').default;
const { load } = require('cheerio');
const { concat, sortBy, uniqWith, slice, differenceWith } = require('lodash');
const { URL, Comparator } = require('../../utils');


/**
 * Beanfun (Taiwan/HongKong) News.
 */
class BeanfunNews extends MarkdownNews {
	constructor(client) {
		super(client, 'beanfun');
	}


	async isOutdated() {
		let result = [];
		const self = this;

		return all ([
			get (process.env.BEANFUN_NEWS_SYSTEM_URL, { responseType: 'text' }),
			get (process.env.BEANFUN_NEWS_UPDATE_URL, { responseType: 'text' }),
			get (process.env.BEANFUN_NEWS_EVENT_URL, { responseType: 'text' }),
		])
			.then (spread ((system, update, event) => {
				const articles = concat([], system.data.MyDataSet.Table, update.data.MyDataSet.Table, event.data.MyDataSet.Table);
				for (const article of articles) {
					result.push ({
						title: article.Title,
						date: new Date (article.StartDate),
					});
				}
			}))
			.then (async () => {
				const cache = await self.retrieveCache();
				result = sortBy (result, Comparator.Article.byDate);
				result = slice (result, Math.max (0, result.length - parseInt(process.env.BEANFUN_NEWS_ARTICLE_LIMIT)));
				return differenceWith (result, cache.articles, Comparator.Article.byTitle).length > 0;
			});
	}


	async retrieve(asJson) {
		return all ([
			get(process.env.BEANFUN_NEWS_SYSTEM_URL, { responseType: 'text' }),
			get(process.env.BEANFUN_NEWS_UPDATE_URL, { responseType: 'text' }),
			get(process.env.BEANFUN_NEWS_EVENT_URL, { responseType: 'text' }),
		])
			.then (spread (async (system, update, event) => {
				let result = [];

				const articles = concat([], system.data.MyDataSet.Table, update.data.MyDataSet.Table, event.data.MyDataSet.Table);
				for (let i = 0; i < articles.length; i++) {
					const article = articles[i];
					const baseUrl = process.env.BEANFUN_NEWS_ARTICLE_URL.replace('{{{ID}}}', article.BullentinId).replace('{{{CAT_ID}}}', article.BullentinCatId);
					const response = await get (baseUrl, { responseType: 'text' });
					const $ = load(response.data);
					const board = $('tbody', $('table.w_table22', '#DataPanel'));
					let thumbnail = process.env.BEANFUN_NEWS_THUMBNAIL_URL;
					$('img[src]', board).each((id, elm) => {
						const src = URL.repairUrl ($(elm).attr('src'), baseUrl);
						if (id === 1) thumbnail = src;
						$(elm).replaceWith (src);
					});
					let contents = $('td[colspan="3"]', 'tr', board);
					contents = contents.html() || '';
					result.push ({
						title: article.Title,
						date: new Date (article.StartDate),
						author: process.env.BEANFUN_NEWS_AUTHOR,
						url: baseUrl,
						icon: process.env.BEANFUN_NEWS_ICON_URL,
						thumbnail: thumbnail,
						content: contents,
					});
				}

				result = uniqWith (result, Comparator.Article.isEquals);
				result = sortBy (result, Comparator.Article.byDate);
				result = slice (result, Math.max (0, result.length - parseInt(process.env.BEANFUN_NEWS_ARTICLE_LIMIT)));
				if (asJson) result = JSON.stringify (result);
				return result;
			}));
	}
}


module.exports = BeanfunNews;
