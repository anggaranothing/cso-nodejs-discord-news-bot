/*
*
*	Libraries.
*
*/
const News = require('../base');
const { get } = require('axios').default;
const { forEach, filter, sortBy, replace, uniqWith, slice } = require('lodash');
const { Comparator } = require('../../utils');
const SteamMarkdownXBBCode = require('../../xbbcodes/steam');
SteamMarkdownXBBCode.init();


/**
 * Steam (CSNS) News.
 */
class SteamNews extends News {
	constructor(client) {
		super(client, 'steam');
	}


	async retrieve(asJson) {
		return get (process.env.STEAM_NEWS_URL)
			.then((response) => {
				let result = [];

				const titlergx = new RegExp(process.env.STEAM_NEWS_TITLE_REGEX);
				forEach (filter(response.data.appnews.newsitems, obj => titlergx.test (obj.title)), function(value) {
					let img = process.env.STEAM_NEWS_THUMBNAIL_URL;
					// Retrieves thumbnail image.
					const imgTagRegex = /\[img\](https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&//=]*))\[\/img\]/i;
					const links = replace (value.contents, /{STEAM_CLAN_IMAGE}/g, process.env.STEAM_NEWS_CLAN_IMAGE_URL);
					const imgSrc = imgTagRegex.exec (links);
					if (imgSrc) img = imgSrc[1];

					// Inserts into result.
					result.push ({
						title: value.title,
						date: new Date (parseInt (value.date) * 1000),
						author: value.author || process.env.STEAM_NEWS_AUTHOR,
						url: value.url,
						icon: process.env.STEAM_NEWS_ICON_URL,
						thumbnail: img,
						content: value.contents,
					});
				});

				result = uniqWith (result, Comparator.Article.isEquals);
				result = sortBy (result, Comparator.Article.byDate);
				result = slice (result, Math.max (0, result.length - parseInt(process.env.STEAM_NEWS_ARTICLE_LIMIT)));
				if (asJson) result = JSON.stringify (result);
				return result;
			});
	}


	toTextMessage(article) {
		return article.url;
	}


	toRichEmbedMessage(article) {
		// Inserts steam clan image base url.
		article.content = replace (article.content, /{STEAM_CLAN_IMAGE}/g, process.env.STEAM_NEWS_CLAN_IMAGE_URL);
	
		// Removes beginning newlines.
		/*const newlineRegex = /(\n+)(?=[^\1])/;
		const newlineResult = newlineRegex.exec (article.content);
		if (newlineResult && newlineResult.index === 0) article.content = replace (article.content, newlineRegex, '');*/

		// Replaces newlines.
		article.content = replace (article.content, /\\n/g, '\n');

		// Converts into markdown.
		article.content = SteamMarkdownXBBCode.process(article.content).markdown;

		return super.toRichEmbedMessage(article);
	}
}


module.exports = SteamNews;
