/*
*
*	Libraries.
*
*/
const parser = require('xbbcode-parser');
const { assign } = require('lodash');


/**
 * BBCode Parser.
 */
class XBBCode {

	/**
	 * Tag parsing callback.
	 *
	 * @callback TagCallback
	 * @param {string} params Tag parameter(s).
	 * @param {string} content Tag content(s).
	 * @return {string} The parsed value.
	 */


	/**
	 * Tag parsing options.
	 *
	 * LIMITIONS on adding NEW TAGS:
	 *
     *  - Tag names should be alphanumeric (including underscores) and all tags should have an opening tag
     *    and a closing tag.
     *  - The [*] tag is an exception because it was already a standard
     *    bbcode tag.
	 *  - Technically tags don't *have* to be alphanumeric, but since
     *    regular expressions are used to parse the text, if you use a non-alphanumeric
     *    tag names, just make sure the tag name gets escaped properly (if needed).
	 *
	 * @typedef TagOptions
	 * @type {object}
	 * @property {TagCallback} openTag
	 *              A function that takes in the tag's parameters (if any) and its
     *              contents, and returns what its HTML open tag should be.
     *              Example: [color=red]test[/color] would take in "=red" as a
     *              parameter input, and "test" as a content input.
     *              It should be noted that any BBCode inside of "content" will have
     *              been processed by the time it enter the openTag function.
     *
     * @property {TagCallback} closeTag
	 *              A function that takes in the tag's parameters (if any) and its
     *              contents, and returns what its HTML close tag should be.
     *
     * @property {boolean=true} displayContent
	 *              If false, the content for the tag will not be displayed.
	 *              This is useful for tags like IMG where its contents
	 *              are actually a parameter input.
     *
     * @property {string[]} restrictChildrenTo
	 *              A list of BBCode tags which are allowed to be nested
     *              within this BBCode tag. If this property is omitted,
     *              any BBCode tag may be nested within the tag.
     *
     * @property {string[]} restrictParentsTo
	 *              A list of BBCode tags which are allowed to be parents of
     *              this BBCode tag. If this property is omitted, any BBCode
     *              tag may be a parent of the tag.
     *
     * @property {boolean=false} noParse
	 *              If true, none of the content WITHIN this tag will be
     *              parsed by the XBBCode parser.
	 */


	/**
	 * Parser options.
	 *
	 * @typedef ParserOptions
	 * @type {object}
	 * @property {string} text The input string to parse.
     * @property {boolean=false} removeMisalignedTags Removes other unparsed tags.
     * @property {boolean=false} addInLineBreaks Adds inline breaker div element.
     * @property {boolean=false} escapeHtml Decodes "[]" HTML codes.
	 */


	/**
	 * Parser result.
	 *
	 * @typedef ParserResult
	 * @type {object}
	 * @property {string} html Result in HTML code.
     * @property {boolean} error Result has parsing error(s).
     * @property {string[]} errorQueue List of error(s).
	 */


	/**
	 * Initializes this parser.
	 * @abstract
	 */
	// eslint-disable-next-line no-empty-function
	static init() {
	}


	/**
	 * Creates a simple tag <tagName></tagName>.
	 * @param {string} tagName The tag name.
	 * @param {TagOptions} options Parsing option(s).
	 * @return {TagOptions}
	 */
	static createSimpleTag(tagName, options) {
		tagName = tagName.trim().toLowerCase();
		return assign ({
			[tagName]: {
				openTag: function() {
					return `<${tagName}>`;
				},
				closeTag: function() {
					return `</${tagName}>`;
				},
			},
		}, options);
	}


	/**
	 * Expose all available tags.
	 * @return {Object<string, TagOptions>}
	 */
	static get tags() {
		return parser.tags;
	}

	/**
	 * Adds a tag.
	 * @param {TagOptions} newtags The new tag. Will overwritten duplicate tags.
	 */
	static addTags(newtags) {
		parser.addTags(newtags);
	}

	/**
	 * Parses the given string.
	 * @param {string} text The string to parse.
	 * @param {ParserOptions} config Parsing option(s).
	 * @return {ParserResult}
	 */
	static process(text, config) {
		config = config || {};
		config.text = config.text || text;
		return parser.process(config);
	}
}


module.exports = XBBCode;
